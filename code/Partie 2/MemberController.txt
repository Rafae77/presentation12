using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Umbraco.Web.Mvc;
using Umbraco.Web.WebApi;
using Umbraco.Core.Models;
using WebApplication2.Model;

namespace WebApplication2.Api
{
    public class MemberController : UmbracoApiController
    {
        [HttpPost]
        [ValidateAntiForgeryToken]
        public bool SubmitLogin(MemberLogin model)
        {
            bool loginSuccesful = false;
            if (ModelState.IsValid)
            {
                if (Membership.ValidateUser(model.Username, model.Password))
                {
                    FormsAuthentication.SetAuthCookie(model.Username, false);
                    loginSuccesful = true;
                }
            }
            return loginSuccesful;
        }
    }
}